<?php

//  New regions for blocks are easy to add. Just put it in here so you have a 
//  new place for extra blocks. The first part is the name of the variable to 
//  place into your page.tpl.php and the second part is the human readable 
//  name that will show up in your blocks administration.

//  You may notice that I have not used the traditional right_sidebar and 
//  left_sidebar. The reason for this is I want people to break the mindset 
//  that the regions are only for sidebars. This theme does use them for such 
//  since I wanted to provide a template that had the proper CSS for 
//  compensating for appearing and disappearing sidebars.

function copyblogger_regions() {
  return array(
    'main_supplements' => t('Primary blocks'),
    'secondary_supplements' => t('Secondary blocks'),
       'content' => t('content'),
       'footer' => t('footer'),
  );
} 

//  Personally, I hate constantly overriding Drupal stock CSS. I find it just 
//  adds to stylesheet bloat when making a completely custom look. That is why 
//  I have disabled most of it with this theme. The only one left is the 
//  admin.css since it really doesn't do a lot beyond formatting the columns 
//  and illustrating the where blocks are located.

//  The format below should be pretty easy to figure out so you can even remove 
//  other module's default styles if you find that they are difficult to 
//  override. (Or for that matter, put one back in if you like the drupal 
//  stuff.) I have tried to put the most crucial styles into this theme's 
//  style.css so that you have a good starting point though. I even included 
//  the .clear-block class sinc ethat is a very handy class to have.

//  One other caveat to this approach is that if you want to use the non-drupal 
//  style method, you have to use copyblogger_styles() in your page.tpl.php 
//  instead of the common $styles veriable. On the plus side, you can simply 
//  use the $styles variable if you don't like my approach here. Choice is good.

function copyblogger_styles() {
  $css = drupal_add_css();
  unset($css['all']['module']['modules/node/node.css']);
  unset($css['all']['module']['modules/system/defaults.css']);
  unset($css['all']['module']['modules/system/system.css']);
  unset($css['all']['module']['modules/user/user.css']);
  return drupal_get_css($css);
}

//  The below function is to construct a set of classes for the body tag. I 
//  will admit, this is pretty much taken from the Zen theme. I have added 
//  some minor changes to make it easier to compensate for additional regions 
//  that you may want to put into your template. I have documented those 
//  further down below.

function _phptemplate_variables($hook, $vars = array()) {
  switch ($hook) {
    // Send a new variable, $logged_in, to page.tpl.php to tell us if the current user is logged in or out.
    case 'page':
      // get the currently logged in user
      global $user;
      
      // An anonymous user has a user id of zero.      
      if ($user->uid > 0) {
        // The user is logged in.
        $vars['logged_in'] = TRUE;
      }
      else {
        // The user has logged out.
        $vars['logged_in'] = FALSE;
      }
      
      $body_classes = array();
      // classes for body element
      // allows advanced theming based on context (home page, node of certain type, etc.)
      $body_classes[] = ($vars['is_front']) ? 'front' : 'not-front';
      $body_classes[] = ($vars['logged_in']) ? 'logged-in' : 'not-logged-in';
      if ($vars['node']->type) {
        $body_classes[] = 'ntype-'. copyblogger_id_safe($vars['node']->type);
      }
      
      //  This following bit of code sets flags for if sidebars exist. If you 
      //  have multiple regions in the sidebars, add them to the conditional. 
      //  The first is for the left side and the second for the right.
      if ($vars['search_box']||$vars['main_supplements']) {
        $leftBar = true;
      }
      
      if ($vars['secondary_supplements']) {
        $rightBar = true;
      }
      
      
      switch (TRUE) {
      	case $leftBar && $rightBar :
      		$body_classes[] = 'sidebars';
      		break;
      	case $leftBar :
      		$body_classes[] = 'main-sidebar';
      		break;
      	case $rightBar :
      		$body_classes[] = 'secondary-sidebar';
      		break;
      }
      // implode with spaces
      $vars['body_classes'] = implode(' ', $body_classes);
      
      break;
      
    case 'node':
      // get the currently logged in user
      global $user;

      //  set a new $is_admin variable
      //  this is determined by looking at the currently logged in user and 
      //  seeing if they are in the role 'admin'
      $vars['is_admin'] = in_array('admin', $user->roles);
      
      $node_classes = array('node');
      if ($vars['sticky']) {
      	$node_classes[] = 'sticky';
      }
      if (!$vars['node']->status) {
      	$node_classes[] = 'node-unpublished';
      }
      $node_classes[] = 'ntype-'. copyblogger_id_safe($vars['node']->type);
      // implode with spaces
      $vars['node_classes'] = implode(' ', $node_classes);
      
      break;
      
    case 'comment':
      // we load the node object that the current comment is attached to
      $node = node_load($vars['comment']->nid);
      // if the author of this comment is equal to the author of the node, we set a variable
      // then in our theme we can theme this comment differently to stand out
      $vars['author_comment'] = $vars['comment']->uid == $node->uid ? TRUE : FALSE;
      break;
  }
  
  return $vars;
}

function copyblogger_id_safe($string) {
  if (is_numeric($string{0})) {
    // if the first character is numeric, add 'n' in front
    $string = 'n'. $string;
  }
  return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
}

//  This bit of code calls in a special template so you can theme the search 
//  box to your liking. Edit search-theme-form.tpl.php to modify the display 
//  and classes.
/*
function phptemplate_search_theme_form($form) {
  return _phptemplate_callback('search-theme-form', array('form' => $form));
}
*/
//  The next three functions sole purpose is to make a better menu. In a 
//  nutshell, it applies "first" and "last" classes like in the primary and 
//  secondary menus.

function phptemplate_menu_tree($pid = 1) {
  if ($tree = phptemplate_menu_tree_improved($pid)) {
    return "\n<ul class=\"menu\">\n". $tree ."</ul>\n";
  }
}

function phptemplate_menu_tree_improved($pid = 1) {
  $menu = menu_get_menu();
  $output = '';

  if (isset($menu['visible'][$pid]) && $menu['visible'][$pid]['children']) {
    $num_children = count($menu['visible'][$pid]['children']);
    for ($i=0; $i < $num_children; ++$i) {
      $mid = $menu['visible'][$pid]['children'][$i];
      $type = isset($menu['visible'][$mid]['type']) ? $menu['visible'][$mid]['type'] : NULL;
      $children = isset($menu['visible'][$mid]['children']) ? $menu['visible'][$mid]['children'] : NULL;
      $extraclass = $i == 0 ? 'first' : ($i == $num_children-1 ? 'last' : '');
      $output .= theme('menu_item', $mid, menu_in_active_trail($mid) || ($type & MENU_EXPANDED) ? theme('menu_tree', $mid) : '', count($children) == 0, $extraclass);     
    }
  }

  return $output;
}

function phptemplate_menu_item($mid, $children = '', $leaf = TRUE, $extraclass = '') {
  return '  <li class="'. ($leaf ? 'leaf' : ($children ? 'expanded' : 'collapsed')) . ($extraclass ? ' ' . $extraclass : '') . '">'. menu_item_link($mid, TRUE, $extraclass) . $children ."</li>\n";
}

//  This is a bit of code so that one can easily edit the code that appears 
//  around all of the comments in a theme. This is not to be confused with 
//  what comment.tpl.php does. That is for individual comments.

function phptemplate_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return "<div id=\"comments\">\n  ". $content . "\n</div>\n";
  }
  else {
    return "<div id=\"comments\">\n  <h2 class=\"comments\">". t('Comments') ."</h2>\n". $content ."\n</div> <!-- /#comments -->\n\n";
  }
}
