<?php

/** 
  This template is intended to be a starting point for almost any sort of XTHML template. The aim is to allow for complete customization with just CSS. This is pretty much impossible due to people wanting certain things in different places, but it should be laid out simply enough so that is easy to move elements. This includes layout and the overall style of the page. I will try to document it as much as possible so that it will be easy to read.  
*/

/**
  The DOCTYPE is very important. It is what tells the browser how to read and render a page. This one specifically is aimed at being XTHML 1.1 Strict. This means that the XHTML standard must be followed. 
*/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>">
  <head>

<?php 
/**
  The meta tags don'[t have as much clout whith search engines anymore, but they are still important. Definitely put as much information as possible and relevant to your your page's content in them.
*/ ?>
<?php print $head; ?>

<?php 
/**
  The page title that appears in the browser bar. Pretty elementary, but I figured I would put a not in here about it for the beginners.
*/ 
?>
<title><?php print $head_title; ?></title>

<?php 
/**
  There are multiple stylesheets here. You will notice that there are 3 media types. These are good to have for the various forms of viewing and proper display on differnet media.
  
  screen - This is the style for your basic computer monitor.
  print - This will be used if a viewer wants to print a page. Very handy for doing printer friendly pages.
  handheld - This is for those new-fangled cellphones and PDAs. Not all units support it as of yet, but it is becoming more prevalent.
*/ 
?>
<?php print $styles; ?>
    
<?php 
/**
  I like to put the JavaScript last since it can have a lot of code that needs to be editted.
*/
?>
<?php print $scripts; ?>

  </head>

  <body class="<?php print $body_classes; ?>">

      <div id="header">
  <?php if ($logo) { ?><div id="logo"><a href="<?php print $base_path ?>" title="<?php print $site_name ?>"><img src="<?php print $logo ?>" alt="<?php print $site_name ?>" /></a></div><?php } ?>
<?php if ($site_name): ?>
       <div id="sitename"><a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a> &nbsp;&nbsp;//&nbsp;&nbsp</div>
<?php endif; ?>

<?php if ($site_slogan): ?>
        <span id="tagline"><?php print $site_slogan; ?></span>
<?php endif; ?>

	<?php if ($search_box): ?>           
		<div id ="searchbox"><?php print $search_box; ?></div>
		<?php endif; ?>

      </div> <!-- /#header -->
    <div id="page" class="clear-block">
      <?php if ($primary_links): ?>
		  <div id="main_menu">
				<?php print theme('links', $primary_links); ?>
			</div> <!-- /#main_menu -->
		<?php endif; ?>



      <div id="wrapper" class="clear-block">

        <div id="subwrapper">

          <div id="container">

            <div id="content">


<?php if ($header || $breadcrumb): ?>
      <div id="breadcrumb">
<?php if ($breadcrumb): ?>
<?php print $breadcrumb; ?>
<?php endif; ?>

<?php if ($header): ?>
<?php print $header; ?>
<?php endif; ?>

      </div> <!-- /#breadcrumb -->

<?php endif; ?>
<?php if ($help): ?>
<?php print $help; ?>

<?php endif; ?>
<?php if ($messages): ?>
<?php print $messages; ?>

<?php endif; ?>
<?php if ($mission): ?>
              <div id="mission">
                <?php print $mission; ?>

              </div>

<?php endif; ?>
<?php if ($content_top):?>
              <div id="content-top">
                <?php print $content_top; ?>

              </div>

<?php endif; ?>
<?php if ($tabs): ?>
              <div class="tabs">
<?php print $tabs; ?>
              </div>

<?php endif; ?>
<?php if ($title): ?>
              <h1 class="title"><?php print $title; ?></h1>

<?php endif; ?>
<?php if ($content): ?>
<?php print $content; ?>
<?php endif; ?>
            </div>

          </div> <!-- /#container -->
<?php if ( $main_supplements): ?>

          <div id="main_supplements">
		<?php print $main_supplements; ?>
          </div> <!-- /#main_supplements -->
<?php endif; ?>

        </div><!-- /#subwrapper -->

<?php if ($secondary_supplements): ?>
        <div id="secondary_supplements">
	
		<?php print $secondary_supplements; ?>
        </div> <!-- /#secondary_supplements -->
<?php endif; ?>

      </div> <!-- /#wrapper -->
	  <?php if ($secondary_links): ?>
      <div id="secondary_menu">
		<?php print theme('links', $secondary_links); ?>
      </div> <!-- /#secondary_menu -->
<?php endif; ?>
    </div> <!-- /#page -->
<?php if ($footer || $footer_message): ?>
      <div id="footer">
<?php if ($footer): ?>
<?php print $footer; ?>

<?php endif; ?>
<?php if ($footer_message): ?>
<?php print $footer_message; ?>
<?php endif; ?>
      </div> <!-- /#footer -->
  
<?php endif; ?>
<div id="copyright">
<a href="http://copyblogger.com">CopyBlogger</a> converted to <a href="http://mydrupal.com">Drupal Themes</a> by MyDrupal.com. Sponsored by: <a href="http://pmzilla.com">PMP</a> | <a href="http://itdiscover.com">IT Discover</a> | <a href="http://techjobs.co.in">Techjobs</a>.
</div>
<?php if ($closure): ?>
<?php print $closure; ?>

<?php endif; ?>
  </body>
</html>